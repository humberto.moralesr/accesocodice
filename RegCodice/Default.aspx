﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RegCodice._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Styles/index.css" rel="stylesheet" type="text/css" />
    <script src="Javascript/acceso.js"></script>
    <script src="Plugins/jQueryMin.js"></script>
    <div class="content-wrapper" id="idContenedor">
   <%-- <section class="content-header">
      <h1>CODICE<small>Registro de entrada</small></h1>
    </section>--%>
    <div class="col-md-12">
      <div class="col-md-8 col-sm-8">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Bienvenido a CODICE</h3>
          </div>
          <div class="box-body">
            <p>Seleccione el tipo de usuario:</p>
            <div class="margin">
              <div class="btn-group">
                <label class="btn btn-default disabled">Usuario</label>
                <select class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                  <%--<option value="1">Ninguno</option>--%>
                  <option value="1">Alumno</option>
                  <option value="2">Maestro</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div id="idFormulario"><br><br>
          <h2>Matricula:</h2>
          <input type="text" maxlength="7" v-model="matricula" id="txtMatricula" class="form-control">
          <br><br>
          <table class="col-xs-12">
            <tr class="col-sm-12">
              <td class="col-sm-4"><input type="button" value="1" class="teclado-numeros" v-on:click="addNumber('1')" />
              </td>
              <td class="col-sm-4"><input type="button" value="2" class="teclado-numeros" v-on:click="addNumber('2')" />
              </td>
              <td class="col-sm-4"><input type="button" value="3" class="teclado-numeros" v-on:click="addNumber('3')" />
              </td>

            </tr>
            <tr class="col-sm-12">
              <td class="col-sm-4"><input type="button" value="4" class="teclado-numeros" v-on:click="addNumber('4')" />
              </td>
              <td class="col-sm-4"><input type="button" value="5" class="teclado-numeros" v-on:click="addNumber('5')" />
              </td>
              <td class="col-sm-4"><input type="button" value="6" class="teclado-numeros" v-on:click="addNumber('6')" />
              </td>

            </tr>
            <tr class="col-sm-12">
              <td class="col-sm-4"><input type="button" value="7" class="teclado-numeros" v-on:click="addNumber('7')" />
              </td>
              <td class="col-sm-4"><input type="button" value="8" class="teclado-numeros" v-on:click="addNumber('8')" />
              </td>
              <td class="col-sm-4"><input type="button" value="9" class="teclado-numeros" v-on:click="addNumber('9')" />
              </td>
            </tr>
            <tr class="col-sm-12">
              <td class="col-sm-4"><input type="button" value="C" class="teclado-botones" v-on:click="matricula = ''" />
              </td>
              <td class="col-sm-4"><input type="button" value="0" class="teclado-numeros" v-on:click="addNumber('0')" />
              </td>
              <td class="col-sm-4"><input type="button" value="ENTER" class="teclado-botones" v-on:click="enviarMatricula()"/></td>
            </tr>
          </table>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 pull-right" style="background-color: beige">
        <div class="col-12">
          <h3><b>Ultimo Usuario</b></h3>
        </div>
        <div class="col-12">
          <div class="col-xs-12">
            <div class="col-xs-4">
              <h4><b>Matricula:</b></h4>
            </div>
            <div class="col-xs-8">
              <h4>{{user.matricula}}</h4>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="col-xs-4">
              <h4><b>Nombre:</b></h4>
            </div>
            <div class="col-xs-8">
              <h4>{{user.name}}</h4>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="col-xs-4">
              <h4><b>Apellido P.:</b></h4>
            </div>
            <div class="col-xs-8">
              <h4>{{user.lastname1}}</h4>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="col-xs-4">
              <h4><b>Apellido M.:</b></h4>
            </div>
            <div class="col-xs-8">
              <h4>{{user.lastname2}}</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

</asp:Content>
