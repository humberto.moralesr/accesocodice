var app;

window.onload = function () {
    app = new Vue({
        el: '#idContenedor',
        data: {
            matricula: "",          //Esta es la matricula que se va a llenar
            tipoUsuario: "1",       //Es el tipo de usuario 1-alumno, 2-maestro
            user:{                  //Aqui tengo la informacion que se va mostrar
                name: "",
                lastname1:"",
                lastname2: "",
                matricula: ""
            }
        },
        methods: {

            enviarMatricula: function () {
                //event.stopPropagation();
                var url = "modulosNexus.aspx/obtenerAlumno";                                        //La direccion   pantalla.aspx/nombreDeLaFuncion
                var jsonData =                                                                      //La informacion que se envia 
                {
                    "matricula"     : this.matricula,
                    "tipoUsuario"   : this.tipoUsuario
                };    
                $.ajax({
                    method: "POST",
                    url: url,
                    data: JSON.stringify(jsonData),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                })
                    .done(function (datos) {
                        datos = datos.d;                            //Cuando se regresa la informacion 
                        var obj = JSON.parse(datos);                //Convierte el string en Json 

                        app.$data.user.matricula = obj.Matricula;   
                        var nombre = obj.Nombre.split("|");
                        app.$data.user.name = nombre[0];
                        app.$data.user.lastname1 = nombre[1];
                        app.$data.user.lastname2 = nombre[2];
                        app.$data.matricula = "";
                        $("#txtMatricula").focus();
                        if(obj.Matricula != "")
                            setTimeout('app.enviarMatricula()', 7000);
                    });
            },

            addNumber: function(number){
                if(this.matricula.length < 8){      //No se pueden agregar mas de 8 digitos
                    this.matricula += number;
                }
                $("#txtMatricula").focus();         //Siempre pone el focus en el campo de la matricula
            }
        }
    })

    
    $("#txtMatricula").focus();
    $('body').keypress(function (e) {
        if (e.which == 13) {
            app.enviarMatricula();      //Si es enter envia la matricula
        }
        var key = window.event ? e.which : e.keyCode;
        if (key < 48 || key > 57) {                     //Filtra para que solo se usen los numeros y que no envien cosas nulas
            //Usando la definición del DOM level 2, "return" NO funciona.
            e.preventDefault();
        }
    });
}

