﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Driver;

namespace RegCodice.Clases
{
    public class MongoDataService
    {
        private MongoServer server;
        private string database { get; set; }

        public MongoDataService(string connectionString)
        {
            MongoClient client = new MongoClient(connectionString);
            server = client.GetServer();
        }

        //Esta funcion es para obtener informacion de la bd, no la use pero aqui esta ¯\_(ツ)_/¯
        public string select(string databaseName, string collectionName, string query)
        {
            try
            {
                var db = server.GetDatabase(databaseName);
                var collection = db.GetCollection(collectionName);
                BsonDocument bsonDoc = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(query);

                var result = collection.FindOne(new QueryDocument(bsonDoc));

                if(result != null)
                {
                    return result.ToJson();
                }
                else
                {
                    return "";
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}