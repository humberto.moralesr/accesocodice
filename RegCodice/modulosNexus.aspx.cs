﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using UANL.Nexus.ProxyService.SIASE.ServicioRegCODICESIASE;
using System.Web.Script.Serialization;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Web.Configuration;

namespace RegCodice.Pantallas
{
    public partial class modulosNexus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                return;
            }
        }

        [WebMethod]
        public static string obtenerAlumno(string matricula, string tipoUsuario)
        {
            string respuesta = "";

            IServicioRegCODICESIASE servicioRegCODICESIASE = new ServicioRegCODICESIASE();              
            var res = servicioRegCODICESIASE.ObtenerInformacionUsuario(tipoUsuario, matricula);     //Se le manda el tipo de usuario y nos regresa la informacion

            if(res.Matricula != "")                                                                 //Si la respuesta es diferente de nulla
                guardarEntrada(res.Matricula, res.Estatus.ToString(), res.Sexo);                    //guardamos la informacion en Mongo

            respuesta = new JavaScriptSerializer().Serialize(res);                                  //Se serializa la informacion

            return respuesta;                                                                       //retornamos la informacion 
        }

        public static void guardarEntrada(string matricula, string estatus, string sexo)
        {
            var dbName = "pruebas";                                                         //Se establece el nombre de nuestra base de datos
            var collectionName = "usuariosIngresados";                                      //Se establece el nombre de la coleccion a la que vamos a acceder
            var conection = WebConfigurationManager.ConnectionStrings["MongoDB"].ToString();
            var client = new MongoClient(conection);
            var database = client.GetDatabase(dbName);                                      //Obtenemos nuestra BD
            var collection = database.GetCollection<BsonDocument>(collectionName);          //Buscamos la coleccion en nuestra BD
            var today = DateTime.Today;

            var document = new BsonDocument                                                 //Creamos nuestro objeto con la informacion que vamos a ingresar 
            {
                {"Matricula", matricula },                                                  //Si ocupamos mas lo mandamos como parametro y lo agregamos aqui
                {"Estatus", estatus },
                {"Sexo", sexo },
                {"Fecha", today.ToShortDateString() }
            };

            collection.InsertOneAsync(document);                                            //Aqui se inserta en la coleccion
        }
    }
}